# Le dépôt de code pour oldies de sous-sol

Ce dépot de sources est constitué d'exemples de codes et de patrons de conception
que j'utilise ou que j'ai utilisé régulièrement dans le cadre de projets variés
de jeux vidéos. Les exemples sont essentiellement en C et utilisent la 
bibliothère SGDK puisqu'il m'adonne de passer du temps à aider et supporter des développeurs qui se passionnent pour la programmation de console de jeu rétro. 
SGDK est une librairie de jeu conçue et maintenue par Stephane Dallongeville 
disponible à cette addresse. https://github.com/Stephane-D/SGDK

Je ne prétend aucunement publier ici les meilleures implémentations qui 
soient puisqu'il s'agit d'un spectre assez large de ce qui existe. Beaucoup 
d'exemples dans ce dépôt furent ou peuvent être sujet de maîtrise universitaire. 
Étant donné la largeur du spectre ici, il est probable qu'un oeil innitié, y 
verra des défauts, voire quelques hérésies. Gardons en tête que les exemples
sont avant tout didactiques et destinés à des programmeurs en apprentissage
de tout âge. 

Après tout, comme le dit le bon vieux dicton, "Bon est l'ennemi de 
la perfection". À vouloir être parfait, on ne fait rien.

Que ce dépot vous inspire, et j'espère entre temps vous partager mon 
enthousiasme pour la programmation.

## Comment consulter ce dépôt

Il n'y a pas d'ordre précis pour le parcourir. Il s'agit plus d'une référence 
que d'un livre. À la racine de ce dépôt vous trouverez des répertoires
concernant divers sujets, lesquels j'ajouterai progressivement lorsque j'ai
le temps et l'intérêt d'écrire.

## Structure des exemples

Tous les exemples ont la même structure. 
| artéfact | description |
|-------|--------|
| readme.md | explication de l'exemple |
| inc/ | fichier d'en-tête `.h`. |
| res/ | ressources rescomp pour l'exemple |
| src/ | fichier de code `.c`. |

## Comment tester et compiler les exemples

Pour le moment, je ne fournis pas de Makefile avec les exemples, puisque ma
configuration diffère largement de ce que la communauté utilise actuellement.
Ce que je vous conseille de faire, c'est de vous créer un nouveau projet 
SGDK, selon le type d'environnement que vous utilisez (MarsDev, pur SGDK etc)
et placer les sources des exemples dans le répertoire `/src` et les ressources
dans le répertoire `/res`. 

## Droits, copyright et emmerdement

Tous le contenu de ce dépôt et entière libre de droit sous la liscence 
Creative Common 2.0. Ce qui veut dire, les droits d'utilisation vous sont
accordé sans limite pour faire ce que vous souhaitez, y compris me revendre ce
code via une cartouche de jeu MD à mon insu. La question des droits en 
informatique est complètement stupide. 

À partir du moment que les sources sont ouvertes,
je n'ai aucun moyen d'aller vérifier que vous avez ou n'avez pas utilisé
une portion ou la totalité de celui-ci. Au final, tout comme les DRM, ça 
n'emmerde que ceux ont le respect du droit d'auteur.

Puis, c'est à vos risques et périls, comment saurez-vous que je n'ai
pas laissé intentionnellement un backdoor dans ce code qui semble si annodin? 
Inspectez-le bien et comprennez le bien et servez-vous en pour vous amuser!
