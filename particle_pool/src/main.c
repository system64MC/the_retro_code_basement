#include <genesis.h>
#include "pool.h"
#include "gassert.h"
#include "r_bullet.h"

#define FIRE_TIMER 4

// table des directions de projectile. Y'a moyen de faire mieux et d'utiliser
// des Fixed types. Mais ce n'est pas le sujet de cet exemple.
static const V2s16 directions[8] = (const V2s16[8]) {
    {2, 0}, 
    {1, 1}, 
    {0, 2}, 
    {-1, 1}, 
    {-2, 0}, 
    {-1, -1}, 
    {0, -2}, 
    {1, -1}
};

// on sépare ici le code métier du code technique. 
static void spawnBullets(BulletPool* const pool) {
    static int timer = 0;
    static int d = 0;

    // à tous les 2 trames, un projectile est lancé. 
    if((timer & 1) == 0) {
        NewBulletInPool(pool, 128, 128, directions[d>>1].x, directions[d>>1].y);
        d = (d+1) & 15;
    }

    // à tous les 32 trames, 5 un projectile sont lancés. 
    if((timer & 31) == 0) {
        NewBulletInPool(pool, 128, 128, 4, 0);
        NewBulletInPool(pool, 128, 128, 3, -2);
        NewBulletInPool(pool, 128, 128, 2, -1);
        NewBulletInPool(pool, 128, 128, 3, 2);
        NewBulletInPool(pool, 128, 128, 2, 1);
    }

    timer++;
}

void main() {
    int frameB = 0;
    SPR_init();

    VDP_waitVBlank(TRUE);
    VDP_setEnable(FALSE);
    
    BulletPool* pool = NewBulletPool();
    
    VDP_setPalette(PAL1, pal_main.data);
    VDP_setEnable(TRUE);

    // notre loop principale
	while(TRUE) {
		SYS_doVBlankProcess();
        spawnBullets(pool);
        UpdateBullets(pool);
        SPR_update();
        VDP_showCPULoad();
		// SYS_showFrameLoad(FALSE);
        // frameB++;
        // if(frameB > 1)
            // frameB = 0;
    }
}