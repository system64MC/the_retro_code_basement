#include "pool.h" 
#include "gassert.h"
#include "r_bullet.h"

// -- [ BulletPool ] -----------------------------------------------------------

// NewBulletPool est une fonction constructeur de BulletPool. Cette fonction
// fait une allocation dynamique sur la HEAP. 
BulletPool* NewBulletPool() {
    BulletPool* const pool = (BulletPool*) malloc(sizeof(BulletPool));
    // utiliser des assertions un peu partout dans le code permet de détecter
    // des bugs avant qu'ils ne surviennent. Les assertions peuvent être 
    // désctivée en mode release. 
    //assert(pool != NULL, "couldn't initialize the pool");
    memset(pool, 0, sizeof(BulletPool));

    // on inscrit dans l'empty list toutes les positions vide. Notre liste de 
    // projectile ne contient rien en ce mometn.
    for(u8 i = 0; i < MAX_BULLET; i++) {
        pool->emptyBulletList[i] = &pool->bullets[i];
    }

    // on assigne le pointeur de la pile à la fin de la liste vide, un de plus
    // après le dernier element de sorte que cette valeur nous indique la 
    // quantité d'objet disponible.
    pool->stackTop = MAX_BULLET;

    // ici c'est un peu un hack, ça pourrait  être mieux fait, mais je veux 
    // garder l'exemple simple. 
    //
    // On crée une première sprite dont l'animation est gérée par SGDK
    Sprite* s = SPR_addSprite(&spr_bullet, -16, -16, TILE_ATTR(PAL1, 0, 0, 0));
    pool->bullets[0].sprite = s;

    // @docme
    for(u8 i = 1; i < MAX_BULLET; i++) {
        // les sprite subséquentes à la première qui fut créé se faut assigner 
        // la tuile animée de la première sprite que nous avons créé. Cela
        // sauve beaucoup de temps de transfert, nous n'avons pas besoins de 
        // 60 animation de projectiles. 0x3FC c'est la position de la tuile en 
        // VRAM. (Je pourrais corriger ça dans la prochaine révision de cet
        // exemple).
        Sprite* s = SPR_addSpriteEx(
            &spr_bullet, 
            -16, -16, 
            TILE_ATTR_FULL(PAL1, 0, 0, 0, 0x3FC), 
            s->attribut, 
            SPR_FLAG_FAST_AUTO_VISIBILITY | SPR_FLAG_AUTO_SPRITE_ALLOC
        );        
        
        pool->bullets[i].sprite = s;
    }

    // KLog_U1("available sprites : ", SPR_getNumActiveSprite());

    return pool;
}

// une pointeur double est utilisé ici pour permettre à la fonction de release
// d'assigner à NULL à la place de l'appellant. C'est un excellent
// moyen de ne jamais oublier de le faire.
//
// C'est une bonne pratique de garder la fonction de release proche du 
// constructeur de sorte à ne pas oublier de la modifier en synchro avec
// le constructeur.
void ReleaseBulletPool(BulletPool** pool_dptr) {
    //assert(*pool_dptr != NULL, "BulletPool has already been released.");
    Bullet* const bullets = (*pool_dptr)->bullets;
    
    for(u8 i = 0; i < MAX_BULLET; i++) {
        Bullet* const ptr = &bullets[i];
        if(ptr->sprite) {
            SPR_releaseSprite(ptr->sprite);
        }
    }
    free(*pool_dptr);
    *pool_dptr = NULL;
}

// c'est notre allocateur dédié.
bool NewBulletInPool(BulletPool* const pool, const u16 x, const u16 y, const s16 vx, const s16 vy) {
    if(pool->stackTop == 0) {
        //KLog("bullet limit reached in pool");
        return FALSE;
    }

    // on réduite la taille de la pile, c'est exactement analogue à retirer
    // un élément de cette pile.
    pool->stackTop--;

    // on récupère la prochaine case vide
    Bullet* b = pool->emptyBulletList[pool->stackTop];
    //assert(b->reserved == FALSE, "bullet isn't supposed to be reserved");

    // on prépare notre projectile ainsi que sa sprite
    b->pos = (V2u16){x, y};
    b->vel = (V2s16){vx, vy};
    b->reserved = TRUE;
    return TRUE;
}

// c'est ici que l'on met en garage les projectiles qui ont terminé leur course
// prêt pour le prochain appel.
void ReleaseBullet(BulletPool* const pool, Bullet* const b) {
    //assert(b->reserved == TRUE, "can't release bullet, already released");
    // on enregistre l'adresse du projectile dans la liste vide
    pool->emptyBulletList[pool->stackTop] = b;
    pool->stackTop++;

    // on sort le sprite de l'écran, on ne veut pas le voir, on ne veut pas
    // le libérer des mains de SGDK. C'est innutile si l'on veut spammer l'écran
    // comme un mongol.
    SPR_setPosition(b->sprite, -16, -16);
    // on indique qu'il ne doit plus être traité.
    b->reserved = FALSE;
}

// UpdateBullets mets à jour tous les projectiles. Il faut absolument éviter 
// un appel de fonction pour chaque projectiles, sinon, le cout en performance
// rapidement se faire ressentir.
void UpdateBullets(BulletPool* const pool) {
    Bullet* ptr = &pool->bullets[MAX_BULLET]; // Le ptr pointe à la fin du tableau
    Bullet* ptr2 = &pool->bullets[MAX_BULLET-1]; // le ptr2 pointe à la fin du tableau -1
    const Bullet* const first = pool->bullets; // premier bullet du tableau, index 0
    // Bullet* middle = &pool->bullets[MIDDLE_BULLET];
    
    // j'ai appris cette syntaxe --> de la part d'un sénior en C. Ça se lit 
    // de "ptr à first".  Ne pas se laisser intimider par la notation, 
    // l'équivalent étant: 
    // 
    // 1) while(ptr-- > first) {}
    // 2) while(ptr > first) { ptr--; ... }
    // 
    // En effet, la liste est parcourue à l'envers, c'est un bon pattern à 
    // prendre. Ça permet entre autre de retirer des éléments d'une liste 
    // pendant que nous  la traversons.
    if(frameB == 0)
    {
        // int i = 0;
        while(ptr --> first) {
            // i++;
            // KLog_S1("Itération F0 : ", i);
        if(!ptr->reserved) {
            continue;
        }
        ptr->pos.x += ptr->vel.x;
        ptr->pos.y += ptr->vel.y;
        SPR_setPosition(ptr->sprite, ptr->pos.x, ptr->pos.y);
        // KLog("Test");

        if(ptr->pos.x > 320 || ptr->pos.y > 224) {
            ReleaseBullet(pool, ptr);
        } 

        // KLog("Test2");
        ptr -= 1;
    }
    frameB = 1;
    }
    else
    {
        // int i = 0;
        // ptr--;
        while(ptr2 --> first) {
            // i++;
            // KLog_S1("Itération F1 : ", i);
        if(!ptr2->reserved) {
            continue;
        }
        ptr2->pos.x += ptr2->vel.x;
        ptr2->pos.y += ptr2->vel.y;
        SPR_setPosition(ptr2->sprite, ptr2->pos.x, ptr2->pos.y);

        if(ptr2->pos.x > 320 || ptr2->pos.y > 224) {
            ReleaseBullet(pool, ptr2);
        }
        ptr2 -= 1;
    }
    frameB = 0;
    }

    // if(frameB == 0)
    // while(ptr --> first) {
    //         // i++;
    //         // KLog_S1("Itération F0 : ", i);
    //     if(!ptr->reserved) {
    //         continue;
    //     }
    //     ptr->pos.x += ptr->vel.x;
    //     ptr->pos.y += ptr->vel.y;
    //     SPR_setPosition(ptr->sprite, ptr->pos.x, ptr->pos.y);
    //     // KLog("Test");

    //     if(ptr->pos.x > 320 || ptr->pos.y > 224) {
    //         ReleaseBullet(pool, ptr);
    //     } 

    //     // KLog("Test2");
    //     // ptr -= 2;
    //     frameB = 1;
    // }
    // else
    //     frameB = 0;

    
} 
