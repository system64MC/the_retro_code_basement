# _Pool_ d'objets (ou de particules)

"_et ça n'a surtout rien avoir avec le billard_"
-Un mec nommé PiscineMorte

## Abstract (ou éloge à malloc)

Créer et libérer une grande quantité d'objets sur d'anciens systèmes très limités à toujours été l'objet de fascination extrême, cette affirmation est relativement facile à prouver à en juger la quantité de [démos](http://pouet.net/ "pouet pouet") démontrant cette prouesse. Il s'agit avec raison bien souvent de hauts faits, haute voltige en terme gestion de mémoire dynamique et autre manipulations que me semblent toujours hors de porté, 30 ans plus tard après avoir recopié `10 PRINT "hello, world` sur le TRS-80 de mon père et m'être dit que ça faisait de moi un humain riche.

Les allocations dynamiques se font généralement via les fonctions de la bibliothèque `libc` du module `stdlib` :
 
 * `malloc(...)` : allouer un bloc mémoire sur la pile
 * `calloc(...)` : allouer un bloc mémoire sur la pile et l'innitialiser à zéro. (Ce qui est une bonne stratégie en général).
 * `realloc(...)` : redimensionner un bloc mémoire en allouant un nouveau et libérant l'ancien si ce dernier est trop petit pour la taille demandée.

Pour de plus amples détails vous pouvez consulter https://code-reference.com/c/stdlib.h

Ce qu'on retient surtout c'est que l'usage de `malloc` est généralement à éviter sur une console jeu de vieille génération en dehors des innitialisations, souvent pendant les écrans noirs. Il est à noter aussi que SGDK possède sont propre allocateur, qui est largement plus performant que celui utilisé dans un OS puisqu'il n'a pas à négocier avec un Kernel pour obtenir la précieuse ressource.

## **Myth0l0g13** 

Il y a certains mythes régulièrement entendus et qui semblent descendre de l'olympe : 
 1. "évitez l'allocation dynamique!" 
 2. "`malloc` c'est Satan, la preuve, ça commence par les lettres MAL"
 3. "ce jeu affiche juste 3 sprites, il doit utiliser `malloc` comme un porc XD". (Je me suis toujours demandé c'était quoi un porc ixdé) 

Est-ce que c'est vrai tout ça?

Pour ma part, je me demande plutôt si l'outil utilisé est le bon. Les idées reçues me ferment l'esprit, je préfère rester empirique tester mes hypothèses. Comme il n'y a rien de noir ou blanc, je répondrai donc à ces 3 idée recues :

1. L'exemple que je vous propose ici rend possible l'allocation dynamique.
2. Pour le procès fait à `malloc`, il faut juste éviter de le faire pendant l'exécution de trames animées à 60 fps (ou 50). Est-ce que c'est vrai? Il est possible que cet exemple puisse très bien rouler avec le `malloc` de SGDK sans taxes apparentes. Je ne l'ai pas mesuré, je suis parfois un _lazy ass_ assumé.
3. Je n'ai jamais entendu personne dire ça.

La raison pour laquelle `malloc` est parfois cité à être évité, ce n'est pas tant que `malloc` n'est pas performant, il est au contraire très performant pour le rôle qu'on lui a donné, c'est à dire être un allocateur tout usage capable de nous donner la lune. La raison pour laquelle il est innadapté dans notre cas précis c'est qu'il ne garantie pas une allocation en temps déterministe alors que dans certains jeux, l'ajustement de ce qui se passe dans une trame est fait au rasoir. 

D'ailleurs si vous désirez approndir les raisons du coût d'appel à `malloc`, vous pouvez vous intéresser à la fragmentation de la mémoire et comment un OS partage la mémoire aux processus qu'il gère. Des sujets largement documentés dans les ouvrages de théorie de système d'exploitation et qui ne sont pas nécéssairement pertinent pour la programmation de la MD.

## C'est bien `malloc`, mais l'exemple? 

Cet exemple expose une stratégie d'allocation dédiée à un tâche très spécifique : gérer un espace mémoire dont les objets ont tous la même taille.

Nous allons parler d'_empty list_.

### _Empty list_
L'_empty list_ [EL] (liste de trous) est une [_stack_ (pile)](https://fr.wikipedia.org/wiki/Pile_(informatique)) qui garde en inventaire les trous d'une autre liste que je nommerai ici Liste 1 (L1). La L1 contient les objets réels, par exemple, les balles de Bill Rizer. (hein!?)

![Empty list](./imgs/diagram1.png "Empty List")

Utiliser cette liste pour faire de l'allocation dynamique est un jeu d'enfant. 

Pour faire une allocation, il s'agit de prendre le pointeur sur le dessus de la pile EL pour obtenir un emplacement vide et utiliser cet emplacement pour le nouvel objet. 

Lorsqu'un objet est libéré, il s'agit de marquer son emplacement mémoire comme étant libre et insérer la position vide sur la pile EL.

Lorsque l'EL est vide, nous savons qu'il ne reste plus d'emplacements vides dans L1. 

Miracle.

EL joue alors le rôle de pool d'objets. C'est un bassin de cases mémoires réutilisables. Voilà une sorte d'allocateur de mémoire spécialement conçu pour des objets de même taille agissant sur une plage de mémoire réservée, comme vous le lirez dans le code source, par la **loc**uste **mal**éfique.


## Notes sur le choix d'implémentation

Cette technique de gestion de la mémoire a les avantages suivants :
 * L'ajout d'un élément à L1 se fait en temps O(1), ce qui veut dire en 
   langues naturelles que nous n'avons pas besoin d'itérer sur L1 pour 
   trouver un emplacement vide.
 * Il est possible de pointer sur les objets de L1 de façon persistante, sans 
   devoir se soucier que les éléments pourraient changer de place lorsque 
   des éléments sont ajoutés ou enlevés de L1.

Cette technique a les inconvénients suivants :
 * Le coût en mémoire, il faut garder une EL de taille proportionelle
   à L1. Une liste de pointeurs coûte 4 octets par éléments sur la famille de 
   CPU MC68000.
 * Lorsqu'on parcours L1 pour mettre à jour tous les objets, il faut
   itérer ceux qui ne sont pas réservés. En d'autres mots, lorsque L1 est vide
   on parcours entièrement la liste quand même. Il y a des moyens de mitiger ce 
   problème mais nous garderons ce sujet pour un autre exemple.

Évidemment, pour gérer une liste de projectiles ou de particules il existe 
d'autres méthodes. Je m'intéresse ici à la méthode, pas la finalité de l'exemple.

## Observations et ouvertures

On verra rapidement, en lançant la ROM et grâce à l'indicateur de charge à gauche, qu'à l'approche de 60 projectiles simultanés, que le temps de calcul pour une trame donnée est sévèrement taxé. Pour le moment notre exemple ne fait rien d'autre que déplacer et instancier des projectiles. L'ajout de zone de collisions, de différent motifs pour les projectiles, et la mise à jour du _loading seam_ (les tuiles d'arrière plan) peuvent pousser les capacités de la MD à ses derniers retranchements.

En réduisant la valeur de la constante `MAX_BULLET` (dans `pool.h`), on peut ajuster la charge d'exécution, évidemment cela enlève le qualificatif d'infernalité que certains 
type de SHMUPs se targent d'avoir.

Si nous avions le contrôle du buffer de sprite ce serait probablement possible d'améliorer un peu les performances. Je vous le dit immédiatement, n'y pensez pas, c'est une mauvaise idée, vous devrez tout gérer vous même, puisque SGDK ne saura pas ce que vous faites. D'autant plus qu'il se pourrait que 60 projectiles causent des bottlenecks encore plus importants ailleurs. Bref, **à mort les optimisations prématurées et qu'on leur coupe la tête, MORTE-COUILLE**!

En multiplexant les projectiles soits par d'infernaux sorts incantés via le HBlank ou simplement par flickering, une trame sur deux, il serait possible d'améliorer la situation, mais cette solution rapelle plutôt les limitations de la NES et certains développeurs auront cette pudeur d'éviter de nous rapeller cette époque où les jambes de Bill Rizer se mettre à clignoter lorsque la méchante _Red Falcon Organization_ envoit à la boucherie tous ses
explosifs soldats.

![Bill Rizer](./imgs/bill.gif "Bill Rizer dont les jambes clignotent")

**Lance** : Hey Bill, tes jambes clignotent!

**Bill** : ... même pas peur.


Nonobstant ce fait, Contra: Hard Corps multiplexait les projectiles une trame sur deux et je doute que beaucoup de joueurs l'aient remarqué.

---

Le porc ixdé, c'était pas vrai.