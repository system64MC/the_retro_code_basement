#ifndef H_ASSERT_C4A2A3D0_4D7F_4C4D_827A_1ECB23932391
#define H_ASSERT_C4A2A3D0_4D7F_4C4D_827A_1ECB23932391

#include <genesis.h>

int frameB;

#ifndef RELEASE_BUILD
#define assert(true_cond, msg) if(!(true_cond)) { KLog(msg); SYS_die(msg); }
#else 
#define assert(true_cond, msg) 
#endif/* RELEASE_BUILD */

#endif /* H_ASSERT_C4A2A3D0_4D7F_4C4D_827A_1ECB23932391 */
