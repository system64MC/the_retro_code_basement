#ifndef H_POOL_D7D7F03D_ADD4_4760_8B10_B8C7380DB25A
#define H_POOL_D7D7F03D_ADD4_4760_8B10_B8C7380DB25A

#include <genesis.h>

#define MAX_BULLET 75
#define MIDDLE_BULLET MAX_BULLET / 2



// Bullet représente un projectile à l'écran
typedef struct Bullet {
    // sprite, référence retournée par SGDK lorsqu'une sprite est créée
    Sprite* sprite;
    // position absolue du projectile par rapport à l'origine du plan
    V2u16 pos;
    // velocity est le déplacement durant une trame d'animation
    V2s16 vel;
    // reserved est FALSE si cet object n'est pas utilisé.
    bool reserved;
} Bullet;

// BulletPool gère tous les projectiles à l'écran
typedef struct BulletPool {
    // bulletList est un bloc de mémoire dynamiquement, aloué lorsque ce module est
    // activé, suffisament long pour contenir MAX_BULLET objets.
    Bullet bullets[MAX_BULLET];
    // emptyBulletList est une stack des emplacement vide de la liste liste bulletList 
    Bullet* emptyBulletList[MAX_BULLET];
    // stacktop indique la position du prochain emplacement vide. La valeur
    // de cette variable indique aussi le nombre d'emplacement vide dans la 
    // liste bullets.
    u8 stackTop;
} BulletPool;

extern BulletPool* NewBulletPool();
extern void ReleaseBulletPool(BulletPool** pool_dptr);
extern bool NewBulletInPool(BulletPool* const pool, const u16 x, const u16 y, const s16 vx, const s16 vy);
extern void UpdateBullets(BulletPool* const pool);


#endif /* H_POOL_D7D7F03D_ADD4_4760_8B10_B8C7380DB25A */
